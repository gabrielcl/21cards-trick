# 21 Cards Trick

This project is a computer simulation of the 21 cards magic trick. Generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

See it in action: https://cards-trick-21.firebaseapp.com

![Google audit](audit.png)

## Development server

Run `npm start` (Do not run `ng serve`, because it's needed a proxy implemented in the `npm start` command in order to the development server run) . Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Running unit tests

Some unit tests were implemented in this project. Run `npm run test-browser` to execute the unit tests viewing the specs details in Chrome. Run `npm run test` to execute the tests in a CI/CD environment. Tests use [Karma](https://karma-runner.github.io).

## Build

Run `npm run build` to build the project (Do not run `ng build`, the other command is the oficial command to build the app, running the tests before, ready for CI/CD and building in AOT mode). The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.




