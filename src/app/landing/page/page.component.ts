import { Component, OnInit } from '@angular/core';
import { GlobalState } from 'src/app/util/global-state';
import { Router } from '@angular/router';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {

  constructor(
    private globalState: GlobalState,
    private router: Router,
  ) {
    this.globalState.set('clickedToOpenIntro', false);
   }

  ngOnInit() {}

  openIntro() {
    this.globalState.set('clickedToOpenIntro', true);
    this.router.navigate(['../game']);
  }

}
