export const calcOrientation = () => {
  let orientation = 'portrait';
  if (window.innerWidth >= window.innerHeight) {
    orientation = 'landscape';
  }
  return orientation;
};
