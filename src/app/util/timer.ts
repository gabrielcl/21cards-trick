export function Timer(callback: any, interval: number) {
  let timerId: any;
  function executeAndStartTimer() {
    callback().then(() => {
      timerId = setTimeout(executeAndStartTimer, interval);
    });
  }
  function stop() {
    if (timerId) {
      clearTimeout(timerId);
      timerId = 0;
    }
  }
  function start() {
    if (!timerId) {
      executeAndStartTimer();
    }
  }
  return Object.freeze({
    start,
    stop
  });
}
