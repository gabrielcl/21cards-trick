import * as nanoclone from 'nanoclone';

const clone = nanoclone.default;

export { clone };
