import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class GlobalState {
    data: any = {};
    public constructor() { }

    public set(key: string, value: any) {
        this.data[key] = value;
    }

    public get(key: string) {
        return this.data[key];
    }

    public erase(key: string) {
        this.data[key] = null;
    }
}
