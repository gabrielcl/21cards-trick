import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IntroComponent } from './intro/intro.component';
import { GameComponent } from './game.component';

import { ComponentsModule } from '../components/components.module';
import { GameRoutingModule } from './game-routing.module';
import { GameService } from './game.service';
import { CardTableComponent } from './card-table/card-table.component';
import { HeaderComponent } from './header/header.component';

@NgModule({
  declarations: [
    IntroComponent,
    GameComponent,
    CardTableComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    GameRoutingModule
  ],
  providers: [
    GameService
  ]
})
export class GameModule { }
