import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class GameService {

  constructor(
    private http: HttpClient
  ) { }

  getShuffledDeck() {
    return this.http.get<any>(`${environment.apiUrl}/deck/new/shuffle/`);
  }

  getDeckCards(deckId: string, count: number) {
    return this.http.get<any>(`${environment.apiUrl}/deck/${deckId}/draw/?count=${count}`);
  }

}
