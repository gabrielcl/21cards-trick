import { Card } from '../types/card';
import { clone } from 'src/app/util/clone';

export function pile(cards: Card[], pileTip: number): Card[] | null {
    if (!cards || (cards && cards.length !== 21)) {
        return null;
    }
    if (!pileTip || (pileTip && !(pileTip >= 1 && pileTip <= 3))) {
        return null;
    }
    const $cards = clone(cards);
    if (pileTip !== 2) {
        const rangeTo = 7 * pileTip;
        const rangeFrom = rangeTo - 7;
        const colTipCards = $cards.splice(rangeFrom, 7);
        $cards.splice(7, 0, ...colTipCards);
    }
    return $cards;
}
