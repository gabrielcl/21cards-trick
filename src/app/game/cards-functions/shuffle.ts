import { Card } from '../types/card';
import { clone } from 'src/app/util/clone';

export function shuffle(cards: Card[]): Card[] | null {
    if (!cards || (cards && cards.length !== 21)) {
        return null;
    }
    const $cards = clone(cards);
    const shuffledCards = [];
    while ($cards.length > 0) {
        const num = Math.floor(Math.random() * $cards.length);
        shuffledCards.push($cards.splice(num, 1)[0]);
    }
    return shuffledCards;
}
