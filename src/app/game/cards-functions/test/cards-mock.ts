/* tslint:disable:disable-next-line max-line-length */
const cards = [
    {suit: 'DIAMONDS', code: '7D', value: '7', images: {png: 'https://deckofcardsapi.com/static/img/7D.png', svg: 'https://deckofcardsapi.com/static/img/7D.svg'}, image: 'https://deckofcardsapi.com/static/img/7D.png'},
    {suit: 'DIAMONDS', code: '4D', value: '4', images: {png: 'https://deckofcardsapi.com/static/img/4D.png', svg: 'https://deckofcardsapi.com/static/img/4D.svg'}, image: 'https://deckofcardsapi.com/static/img/4D.png'},
    {suit: 'CLUBS', code: '6C', value: '6', images: {png: 'https://deckofcardsapi.com/static/img/6C.png', svg: 'https://deckofcardsapi.com/static/img/6C.svg'}, image: 'https://deckofcardsapi.com/static/img/6C.png'},
    {suit: 'DIAMONDS', code: 'JD', value: 'JACK', images: {png: 'https://deckofcardsapi.com/static/img/JD.png', svg: 'https://deckofcardsapi.com/static/img/JD.svg'}, image: 'https://deckofcardsapi.com/static/img/JD.png'},
    {suit: 'CLUBS', code: '0C', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0C.png', svg: 'https://deckofcardsapi.com/static/img/0C.svg'}, image: 'https://deckofcardsapi.com/static/img/0C.png'},
    {suit: 'DIAMONDS', code: '6D', value: '6', images: {png: 'https://deckofcardsapi.com/static/img/6D.png', svg: 'https://deckofcardsapi.com/static/img/6D.svg'}, image: 'https://deckofcardsapi.com/static/img/6D.png'},
    {suit: 'DIAMONDS', code: 'QD', value: 'QUEEN', images: {png: 'https://deckofcardsapi.com/static/img/QD.png', svg: 'https://deckofcardsapi.com/static/img/QD.svg'}, image: 'https://deckofcardsapi.com/static/img/QD.png'},
    {suit: 'DIAMONDS', code: 'KD', value: 'KING', images: {png: 'https://deckofcardsapi.com/static/img/KD.png', svg: 'https://deckofcardsapi.com/static/img/KD.svg'}, image: 'https://deckofcardsapi.com/static/img/KD.png'},
    {suit: 'SPADES', code: '0S', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0S.png', svg: 'https://deckofcardsapi.com/static/img/0S.svg'}, image: 'https://deckofcardsapi.com/static/img/0S.png'},
    {suit: 'DIAMONDS', code: '0D', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0D.png', svg: 'https://deckofcardsapi.com/static/img/0D.svg'}, image: 'https://deckofcardsapi.com/static/img/0D.png'},
    {suit: 'DIAMONDS', code: '3D', value: '3', images: {png: 'https://deckofcardsapi.com/static/img/3D.png', svg: 'https://deckofcardsapi.com/static/img/3D.svg'}, image: 'https://deckofcardsapi.com/static/img/3D.png'},
    {suit: 'HEARTS', code: 'JH', value: 'JACK', images: {png: 'https://deckofcardsapi.com/static/img/JH.png', svg: 'https://deckofcardsapi.com/static/img/JH.svg'}, image: 'https://deckofcardsapi.com/static/img/JH.png'},
    {suit: 'CLUBS', code: 'QC', value: 'QUEEN', images: {png: 'https://deckofcardsapi.com/static/img/QC.png', svg: 'https://deckofcardsapi.com/static/img/QC.svg'}, image: 'https://deckofcardsapi.com/static/img/QC.png'},
    {suit: 'SPADES', code: '9S', value: '9', images: {png: 'https://deckofcardsapi.com/static/img/9S.png', svg: 'https://deckofcardsapi.com/static/img/9S.svg'}, image: 'https://deckofcardsapi.com/static/img/9S.png'},
    {suit: 'SPADES', code: '8S', value: '8', images: {png: 'https://deckofcardsapi.com/static/img/8S.png', svg: 'https://deckofcardsapi.com/static/img/8S.svg'}, image: 'https://deckofcardsapi.com/static/img/8S.png'},
    {suit: 'DIAMONDS', code: '2D', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2D.png', svg: 'https://deckofcardsapi.com/static/img/2D.svg'}, image: 'https://deckofcardsapi.com/static/img/2D.png'},
    {suit: 'SPADES', code: '2S', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2S.png', svg: 'https://deckofcardsapi.com/static/img/2S.svg'}, image: 'https://deckofcardsapi.com/static/img/2S.png'},
    {suit: 'CLUBS', code: 'KC', value: 'KING', images: {png: 'https://deckofcardsapi.com/static/img/KC.png', svg: 'https://deckofcardsapi.com/static/img/KC.svg'}, image: 'https://deckofcardsapi.com/static/img/KC.png'},
    {suit: 'HEARTS', code: '3H', value: '3', images: {png: 'https://deckofcardsapi.com/static/img/3H.png', svg: 'https://deckofcardsapi.com/static/img/3H.svg'}, image: 'https://deckofcardsapi.com/static/img/3H.png'},
    {suit: 'HEARTS', code: '2H', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2H.png', svg: 'https://deckofcardsapi.com/static/img/2H.svg'}, image: 'https://deckofcardsapi.com/static/img/2H.png'},
    {suit: 'HEARTS', code: 'AH', value: 'ACE', images: {png: 'https://deckofcardsapi.com/static/img/AH.png', svg: 'https://deckofcardsapi.com/static/img/AH.svg'}, image: 'https://deckofcardsapi.com/static/img/AH.png'}
];

// column 1 is swapped with column 2
const swipedCards1 = [
    {suit: 'DIAMONDS', code: 'KD', value: 'KING', images: {png: 'https://deckofcardsapi.com/static/img/KD.png', svg: 'https://deckofcardsapi.com/static/img/KD.svg'}, image: 'https://deckofcardsapi.com/static/img/KD.png'},
    {suit: 'SPADES', code: '0S', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0S.png', svg: 'https://deckofcardsapi.com/static/img/0S.svg'}, image: 'https://deckofcardsapi.com/static/img/0S.png'},
    {suit: 'DIAMONDS', code: '0D', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0D.png', svg: 'https://deckofcardsapi.com/static/img/0D.svg'}, image: 'https://deckofcardsapi.com/static/img/0D.png'},
    {suit: 'DIAMONDS', code: '3D', value: '3', images: {png: 'https://deckofcardsapi.com/static/img/3D.png', svg: 'https://deckofcardsapi.com/static/img/3D.svg'}, image: 'https://deckofcardsapi.com/static/img/3D.png'},
    {suit: 'HEARTS', code: 'JH', value: 'JACK', images: {png: 'https://deckofcardsapi.com/static/img/JH.png', svg: 'https://deckofcardsapi.com/static/img/JH.svg'}, image: 'https://deckofcardsapi.com/static/img/JH.png'},
    {suit: 'CLUBS', code: 'QC', value: 'QUEEN', images: {png: 'https://deckofcardsapi.com/static/img/QC.png', svg: 'https://deckofcardsapi.com/static/img/QC.svg'}, image: 'https://deckofcardsapi.com/static/img/QC.png'},
    {suit: 'SPADES', code: '9S', value: '9', images: {png: 'https://deckofcardsapi.com/static/img/9S.png', svg: 'https://deckofcardsapi.com/static/img/9S.svg'}, image: 'https://deckofcardsapi.com/static/img/9S.png'},
    {suit: 'DIAMONDS', code: '7D', value: '7', images: {png: 'https://deckofcardsapi.com/static/img/7D.png', svg: 'https://deckofcardsapi.com/static/img/7D.svg'}, image: 'https://deckofcardsapi.com/static/img/7D.png'},
    {suit: 'DIAMONDS', code: '4D', value: '4', images: {png: 'https://deckofcardsapi.com/static/img/4D.png', svg: 'https://deckofcardsapi.com/static/img/4D.svg'}, image: 'https://deckofcardsapi.com/static/img/4D.png'},
    {suit: 'CLUBS', code: '6C', value: '6', images: {png: 'https://deckofcardsapi.com/static/img/6C.png', svg: 'https://deckofcardsapi.com/static/img/6C.svg'}, image: 'https://deckofcardsapi.com/static/img/6C.png'},
    {suit: 'DIAMONDS', code: 'JD', value: 'JACK', images: {png: 'https://deckofcardsapi.com/static/img/JD.png', svg: 'https://deckofcardsapi.com/static/img/JD.svg'}, image: 'https://deckofcardsapi.com/static/img/JD.png'},
    {suit: 'CLUBS', code: '0C', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0C.png', svg: 'https://deckofcardsapi.com/static/img/0C.svg'}, image: 'https://deckofcardsapi.com/static/img/0C.png'},
    {suit: 'DIAMONDS', code: '6D', value: '6', images: {png: 'https://deckofcardsapi.com/static/img/6D.png', svg: 'https://deckofcardsapi.com/static/img/6D.svg'}, image: 'https://deckofcardsapi.com/static/img/6D.png'},
    {suit: 'DIAMONDS', code: 'QD', value: 'QUEEN', images: {png: 'https://deckofcardsapi.com/static/img/QD.png', svg: 'https://deckofcardsapi.com/static/img/QD.svg'}, image: 'https://deckofcardsapi.com/static/img/QD.png'},
    {suit: 'SPADES', code: '8S', value: '8', images: {png: 'https://deckofcardsapi.com/static/img/8S.png', svg: 'https://deckofcardsapi.com/static/img/8S.svg'}, image: 'https://deckofcardsapi.com/static/img/8S.png'},
    {suit: 'DIAMONDS', code: '2D', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2D.png', svg: 'https://deckofcardsapi.com/static/img/2D.svg'}, image: 'https://deckofcardsapi.com/static/img/2D.png'},
    {suit: 'SPADES', code: '2S', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2S.png', svg: 'https://deckofcardsapi.com/static/img/2S.svg'}, image: 'https://deckofcardsapi.com/static/img/2S.png'},
    {suit: 'CLUBS', code: 'KC', value: 'KING', images: {png: 'https://deckofcardsapi.com/static/img/KC.png', svg: 'https://deckofcardsapi.com/static/img/KC.svg'}, image: 'https://deckofcardsapi.com/static/img/KC.png'},
    {suit: 'HEARTS', code: '3H', value: '3', images: {png: 'https://deckofcardsapi.com/static/img/3H.png', svg: 'https://deckofcardsapi.com/static/img/3H.svg'}, image: 'https://deckofcardsapi.com/static/img/3H.png'},
    {suit: 'HEARTS', code: '2H', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2H.png', svg: 'https://deckofcardsapi.com/static/img/2H.svg'}, image: 'https://deckofcardsapi.com/static/img/2H.png'},
    {suit: 'HEARTS', code: 'AH', value: 'ACE', images: {png: 'https://deckofcardsapi.com/static/img/AH.png', svg: 'https://deckofcardsapi.com/static/img/AH.svg'}, image: 'https://deckofcardsapi.com/static/img/AH.png'}
];

// column 3 is swapped with column 2
const swipedCards3 = [
    {suit: 'DIAMONDS', code: '7D', value: '7', images: {png: 'https://deckofcardsapi.com/static/img/7D.png', svg: 'https://deckofcardsapi.com/static/img/7D.svg'}, image: 'https://deckofcardsapi.com/static/img/7D.png'},
    {suit: 'DIAMONDS', code: '4D', value: '4', images: {png: 'https://deckofcardsapi.com/static/img/4D.png', svg: 'https://deckofcardsapi.com/static/img/4D.svg'}, image: 'https://deckofcardsapi.com/static/img/4D.png'},
    {suit: 'CLUBS', code: '6C', value: '6', images: {png: 'https://deckofcardsapi.com/static/img/6C.png', svg: 'https://deckofcardsapi.com/static/img/6C.svg'}, image: 'https://deckofcardsapi.com/static/img/6C.png'},
    {suit: 'DIAMONDS', code: 'JD', value: 'JACK', images: {png: 'https://deckofcardsapi.com/static/img/JD.png', svg: 'https://deckofcardsapi.com/static/img/JD.svg'}, image: 'https://deckofcardsapi.com/static/img/JD.png'},
    {suit: 'CLUBS', code: '0C', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0C.png', svg: 'https://deckofcardsapi.com/static/img/0C.svg'}, image: 'https://deckofcardsapi.com/static/img/0C.png'},
    {suit: 'DIAMONDS', code: '6D', value: '6', images: {png: 'https://deckofcardsapi.com/static/img/6D.png', svg: 'https://deckofcardsapi.com/static/img/6D.svg'}, image: 'https://deckofcardsapi.com/static/img/6D.png'},
    {suit: 'DIAMONDS', code: 'QD', value: 'QUEEN', images: {png: 'https://deckofcardsapi.com/static/img/QD.png', svg: 'https://deckofcardsapi.com/static/img/QD.svg'}, image: 'https://deckofcardsapi.com/static/img/QD.png'},
    {suit: 'SPADES', code: '8S', value: '8', images: {png: 'https://deckofcardsapi.com/static/img/8S.png', svg: 'https://deckofcardsapi.com/static/img/8S.svg'}, image: 'https://deckofcardsapi.com/static/img/8S.png'},
    {suit: 'DIAMONDS', code: '2D', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2D.png', svg: 'https://deckofcardsapi.com/static/img/2D.svg'}, image: 'https://deckofcardsapi.com/static/img/2D.png'},
    {suit: 'SPADES', code: '2S', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2S.png', svg: 'https://deckofcardsapi.com/static/img/2S.svg'}, image: 'https://deckofcardsapi.com/static/img/2S.png'},
    {suit: 'CLUBS', code: 'KC', value: 'KING', images: {png: 'https://deckofcardsapi.com/static/img/KC.png', svg: 'https://deckofcardsapi.com/static/img/KC.svg'}, image: 'https://deckofcardsapi.com/static/img/KC.png'},
    {suit: 'HEARTS', code: '3H', value: '3', images: {png: 'https://deckofcardsapi.com/static/img/3H.png', svg: 'https://deckofcardsapi.com/static/img/3H.svg'}, image: 'https://deckofcardsapi.com/static/img/3H.png'},
    {suit: 'HEARTS', code: '2H', value: '2', images: {png: 'https://deckofcardsapi.com/static/img/2H.png', svg: 'https://deckofcardsapi.com/static/img/2H.svg'}, image: 'https://deckofcardsapi.com/static/img/2H.png'},
    {suit: 'HEARTS', code: 'AH', value: 'ACE', images: {png: 'https://deckofcardsapi.com/static/img/AH.png', svg: 'https://deckofcardsapi.com/static/img/AH.svg'}, image: 'https://deckofcardsapi.com/static/img/AH.png'},
    {suit: 'DIAMONDS', code: 'KD', value: 'KING', images: {png: 'https://deckofcardsapi.com/static/img/KD.png', svg: 'https://deckofcardsapi.com/static/img/KD.svg'}, image: 'https://deckofcardsapi.com/static/img/KD.png'},
    {suit: 'SPADES', code: '0S', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0S.png', svg: 'https://deckofcardsapi.com/static/img/0S.svg'}, image: 'https://deckofcardsapi.com/static/img/0S.png'},
    {suit: 'DIAMONDS', code: '0D', value: '10', images: {png: 'https://deckofcardsapi.com/static/img/0D.png', svg: 'https://deckofcardsapi.com/static/img/0D.svg'}, image: 'https://deckofcardsapi.com/static/img/0D.png'},
    {suit: 'DIAMONDS', code: '3D', value: '3', images: {png: 'https://deckofcardsapi.com/static/img/3D.png', svg: 'https://deckofcardsapi.com/static/img/3D.svg'}, image: 'https://deckofcardsapi.com/static/img/3D.png'},
    {suit: 'HEARTS', code: 'JH', value: 'JACK', images: {png: 'https://deckofcardsapi.com/static/img/JH.png', svg: 'https://deckofcardsapi.com/static/img/JH.svg'}, image: 'https://deckofcardsapi.com/static/img/JH.png'},
    {suit: 'CLUBS', code: 'QC', value: 'QUEEN', images: {png: 'https://deckofcardsapi.com/static/img/QC.png', svg: 'https://deckofcardsapi.com/static/img/QC.svg'}, image: 'https://deckofcardsapi.com/static/img/QC.png'},
    {suit: 'SPADES', code: '9S', value: '9', images: {png: 'https://deckofcardsapi.com/static/img/9S.png', svg: 'https://deckofcardsapi.com/static/img/9S.svg'}, image: 'https://deckofcardsapi.com/static/img/9S.png'},
];

export { cards, swipedCards1, swipedCards3 };
