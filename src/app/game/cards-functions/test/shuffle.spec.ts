import { shuffle } from '../shuffle';
import { cards } from './cards-mock';
import { clone } from 'src/app/util/clone';

describe('shuffle()', () => {

    let cardsMock: any;

    beforeEach( () => {
        cardsMock = clone(cards);
    });

    it('should pass an array of 21 cards on "cards" argument', () => {
        const c = cardsMock.pop();
        expect(shuffle(cardsMock)).not.toBeTruthy();
    });

    it('should get a rearranged deck of cards after shuffle, with 21 cards', () => {
        const shuffledCards = shuffle(cardsMock);
        expect(shuffledCards).not.toEqual(cardsMock);
        expect(shuffledCards.length).toEqual(21);
    });

});
