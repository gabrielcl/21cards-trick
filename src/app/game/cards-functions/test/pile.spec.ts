import { pile } from '../pile';
import { cards, swipedCards1, swipedCards3 } from './cards-mock';
import { clone } from 'src/app/util/clone';

describe('pile()', () => {

    let cardsMock: any;

    beforeEach( () => {
        cardsMock = clone(cards);
    });

    it('should pass an array of 21 cards on "cards" argument', () => {
        const c = cardsMock.pop();
        expect(pile(cardsMock, 3)).not.toBeTruthy();
    });

    it('should pass a value in range from 1 to 3 on "pileTip" argument', () => {
        expect(pile(cardsMock, 4)).not.toBeTruthy();
    });

    it('should put cards of "pileTip" argument column on the middle of the deck', () => {
        expect(pile(cardsMock, 1)).toEqual(swipedCards1);
        expect(pile(cardsMock, 2)).toEqual(cardsMock);
        expect(pile(cardsMock, 3)).toEqual(swipedCards3);
    });

});
