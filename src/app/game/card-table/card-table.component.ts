import {
  Component,
  OnInit,
  Input,
  SimpleChanges,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter,
  OnChanges,
} from '@angular/core';
import { GameStage } from '../types/game-stage';

@Component({
  selector: 'app-card-table',
  templateUrl: './card-table.component.html',
  styleUrls: ['./card-table.component.scss']
})
export class CardTableComponent implements OnInit, OnChanges {

  @ViewChild('tableElm') tableElm!: ElementRef;

  @Input() orientation!: string;
  currentOrientation!: string;

  @Input() stage!: GameStage;
  Stages = GameStage;

  @Input() cardsNewMatch: any;
  cards: any;
  cardsRows = [];

  @Input() animateCardsBackToPilesEvtEmitter!: EventEmitter<boolean>;

  @Input() cardChoosenGuessedSrc = '';

  @Output() haveChoosenPile: EventEmitter<any> = new EventEmitter<any>();
  @Output() haveClickedOk: EventEmitter<boolean> = new EventEmitter<boolean>();

  ngOnChanges(changes: SimpleChanges) {
    if (changes.orientation) {
      this.currentOrientation = changes.orientation.currentValue;
    }
    if (changes.cardsNewMatch) {
      this.cards = changes.cardsNewMatch.currentValue;
      this.putCardsOnPiles();
    }
    if (changes.animateCardsBackToPilesEvtEmitter) {
      this.animateCardsBackToPilesEvtEmitter.subscribe( (_: any) => {
        this.animateCardsBackToPiles();
      });
    }
  }

  constructor(
    private elRef: ElementRef
  ) { }

  ngOnInit() {

  }

  putCardsOnPiles() {
    this.cardsRows = this.distributeCardsRows();
    setTimeout( () => {
      this.elRef.nativeElement.querySelectorAll(`.card`).forEach( (elm: HTMLElement, index: number) => {
        setTimeout( () => {
          elm.classList.remove('card--is-piled');
        }, 20 * (index + 1));
      });
    }, 1000);
  }

  animateCardsBackToPiles() {
    this.elRef.nativeElement.querySelectorAll(`.card`).forEach( (elm: HTMLElement, index: number) => {
      setTimeout( () => {
        elm.classList.add('card--is-piled');
      }, 20 * (index + 1));
    });
  }

  choosePile(num: number) {
    this.animateCardsBackToPiles();
    const cards = this.joinCardsRows();
    this.haveChoosenPile.emit({
      pileNumber: num,
      cards
    });
  }

  distributeCardsRows(): any {
    const cardsRows: any[][] = [];
    cardsRows[0] = [];
    cardsRows[1] = [];
    cardsRows[2] = [];
    let i = 2;
    this.cards.cards.forEach( (card: any) => {
      if (i < 0) {
        i = 2;
      }
      cardsRows[i].push(card);
      i--;
    });
    return cardsRows;
  }

  joinCardsRows() {
    const cards: any[] = [];
    this.cardsRows.forEach( (row: any[]) => {
      row.forEach( (card) => {
        cards.push(card);
      });
    });
    return cards;
  }

}
