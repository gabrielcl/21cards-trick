export enum GameStage {
    Intro = 1,
    PickCard,
    Guess1,
    Guess2,
    Guess3,
    End
}
