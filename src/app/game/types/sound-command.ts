export interface SoundCommand {
    action: string;
    sound: string;
    miliseconds?: number;
}
