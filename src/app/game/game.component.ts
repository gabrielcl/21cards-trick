import { Component, OnInit, HostListener, EventEmitter } from '@angular/core';

import { GameStage } from './types/game-stage';
import { calcOrientation } from '../util/calc-orientation';
import { SoundCommand } from './types/sound-command';
import { Card } from './types/card';
import { GameService } from './game.service';
import { gameConfig } from './game.config';
import { GlobalState } from '../util/global-state';
import { Router } from '@angular/router';
import { pile, shuffle } from './cards-functions';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  stage: GameStage = GameStage.Intro;
  Stages = GameStage;
  currentOrientation!: string;
  soundCommand: EventEmitter<SoundCommand> = new EventEmitter<SoundCommand>();
  soundPlayerReady = {status: false};
  typeTimer: any;
  loadingStatus!: number;
  loadingStatusMessage: string[] = [
    '',
    'Loading audio...',
    'Loading cards...',
    'Hi, I\'m the Magician Robot! Press "OK" to init the trick...',
    'Sorry :( there was an error while fetching the cards. Try again later!',
    'Sorry :( there was an unknown error!',
  ];
  gameCurrentMessage = 0;
  gameMessage: string[] = [
    '',
    'Choose a card (Don\'t click it), memorize it and click on "Ok".',
    'Shuffling...',
    'Now, click on the number of the row/column where your card is located.',
    'Again, click on the number of the row/column where your card is located.',
    'One last time, click on the number of the row/column where your card is located.',
    'Now I\'ll show the card you\'ve choose',
    'Bingo!',
    'Would you like to try again? (If so, click "Ok")'
  ];

  deckOfCards: Card[] = [];
  cardsSendToTable: any;
  cardsBackFromTable: any;
  pileClicked!: number;
  animateCardsBackToPilesEvtEmitter!: EventEmitter<boolean>;
  cardChoosenGuessedSrc = '';

  @HostListener('window:resize') onResize() {
    this.setOrientation();
  }

  constructor(
    private gameService: GameService,
    private globalState: GlobalState,
    private router: Router
    ) {
      if (!this.globalState.get('clickedToOpenIntro')) {
        this.router.navigate(['../']);
      }
     }

  ngOnInit() {
    this.playIntroSound();
    this.setOrientation();
    this.consoleLoadingLog(1);
    try {
      const deckItem: any = localStorage.getItem('deck');
      if (deckItem) {
        const deck: Card[] = JSON.parse(deckItem as string);
        this.deckOfCards = deck;
        this.prepareToInitGame();
        return;
      }
      this.getShuffledDeck();
    } catch (_) {
      this.consoleLoadingLog(5);
    }
  }

  getShuffledDeck() {
    this.gameService.getShuffledDeck().subscribe(
      (res) => {
        this.getDeckCards(res.deck_id);
      },
      (_) => {
        this.consoleLoadingLog(4);
      }
    );
  }

  getDeckCards(deckId: any) {
    this.gameService.getDeckCards(deckId, gameConfig.qttCards).subscribe(
      (res) => {
        this.deckOfCards = res.cards;
        localStorage.setItem('deck', JSON.stringify(this.deckOfCards));
        this.prepareToInitGame();
      },
      (_) => {
        this.consoleLoadingLog(4);
      }
    );
  }

  prepareToInitGame() {
    this.consoleLoadingLog(2);
    setTimeout( () => {
      this.consoleLoadingLog(3);
    }, 2000);
  }

  consoleLoadingLog(loadingStatus: number) {
    this.loadingStatus = loadingStatus;
    const message = this.loadingStatusMessage[loadingStatus];
    this.consoleType(message);
  }

  initGame() {
    this.stopIntroSound();
    this.stopTypeSound();
    this.nextGameStage();
  }

  nextGameStage() {
    this.stage++;
    if (this.stage === GameStage.PickCard) {
      this.animateCardsBackToPilesEvtEmitter = new EventEmitter<boolean>();
      const shuffledCards = shuffle(this.deckOfCards);
      this.putCards(shuffledCards as Card[]);
      this.gameCurrentMessage = 1;
      this.consoleType(this.gameMessage[this.gameCurrentMessage]);
      return;
    }
    if (
      this.stage === GameStage.Guess1
      || this.stage === GameStage.Guess2
      || this.stage === GameStage.Guess3
      || this.stage === GameStage.End
    ) {
      this.gameCurrentMessage = 2;
      this.consoleType(this.gameMessage[this.gameCurrentMessage]);
      this.animatePutCardsBackToPiles();
      let reorganizedCards: any;
      (this.stage === GameStage.Guess1) ?
        reorganizedCards = shuffle(this.cardsSendToTable.cards) :
        reorganizedCards = pile(this.cardsBackFromTable.cards, this.cardsBackFromTable.pileNumber);
      setTimeout( () => {
        this.gameCurrentMessage = this.stage;
        this.consoleType(this.gameMessage[this.gameCurrentMessage]);
        (this.stage === GameStage.End) ?
          this.inferCardChoosen(reorganizedCards) :
          this.putCards(reorganizedCards);
      }, 2000);
      return;
    }
    this.cardChoosenGuessedSrc = '';
    this.stage = 1;
    this.nextGameStage();
  }

  putCards(cards: Card[]) {
    this.cardsSendToTable = {
      cards,
      stage: this.stage
    };
  }

  animatePutCardsBackToPiles() {
    this.animateCardsBackToPilesEvtEmitter.emit(true);
  }

  consoleButtonAction() {
    this.nextGameStage();
  }

  choosePileButtonAction(piles: any) {
    this.cardsBackFromTable = piles;
    this.nextGameStage();
  }

  inferCardChoosen(reorganizedCards: Card[]) {
    setTimeout( () => {
      this.gameCurrentMessage = 7;
      const msg = this.gameMessage[this.gameCurrentMessage];
      this.consoleType(msg);
      this.cardChoosenGuessedSrc = reorganizedCards[gameConfig.guessPosition].image;
      this.finishMagic();
    }, 3000);
  }

  finishMagic() {
    setTimeout( () => {
      this.gameCurrentMessage = 8;
      const msg = this.gameMessage[this.gameCurrentMessage];
      this.consoleType(msg);
    }, 2000);
  }

  setOrientation() {
    const orientation = calcOrientation();
    if (orientation !== this.currentOrientation) {
      this.currentOrientation = orientation;
    }
  }

  consoleType(message: string) {
    const time = message.length * gameConfig.typingSpeed;
    const ready = this.soundPlayerReady;
    if (!ready.status) {
      const interval = setInterval(() => {
        if (ready.status) {
          clearInterval(interval);
          this.consoleType(message);
        }
      }, 50);
      return;
    }
    this.soundCommand.emit( {
      action: 'play',
      sound: 'type',
      miliseconds: time
    });
  }

  playIntroSound(miliseconds?: number) {
    const ready = this.soundPlayerReady;
    if (!ready.status) {
      const interval = setInterval(() => {
        if (ready.status) {
          clearInterval(interval);
          this.playIntroSound(miliseconds);
        }
      }, 50);
      return;
    }
    this.soundCommand.emit( {
      action: 'play',
      sound: 'intro',
      miliseconds
    } );
  }

  stopIntroSound() {
    this.soundCommand.emit( {
      action: 'stop',
      sound: 'intro'
    } );
  }

  playClickSound() {
    this.soundCommand.emit( {
      action: 'play',
      sound: 'click'
    } );
  }

  stopTypeSound() {
    this.soundCommand.emit( {
      action: 'stop',
      sound: 'type'
    } );
  }

}
