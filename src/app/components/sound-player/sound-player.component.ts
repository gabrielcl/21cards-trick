import {Component,
        OnInit,
        Input,
        ViewChild,
        ElementRef,
        SimpleChanges,
        OnChanges,
        EventEmitter,
        Output} from '@angular/core';
import { SoundCommand } from 'src/app/game/types/sound-command';

@Component({
  selector: 'app-sound-player',
  templateUrl: './sound-player.component.html',
  styleUrls: ['./sound-player.component.scss']
})
export class SoundPlayerComponent implements OnInit , OnChanges {

  audioEventEmitterInitialized = false;
  @Output() ready: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Input() action!: EventEmitter<SoundCommand>;
  @ViewChild('clickSound') clickSound!: ElementRef;
  @ViewChild('introSound') introSound!: ElementRef;
  @ViewChild('typeSound') typeSound!: ElementRef;

  ngOnChanges(changes: SimpleChanges) {
    if (!this.audioEventEmitterInitialized && changes.action) {
      this.audioEventEmitterInitialized = true;
      this.action.subscribe( (action: SoundCommand) => {
        this.audioAction(action);
      });
      this.ready.emit(true);
    }
  }

  ngOnInit() {}

  audioAction(command: SoundCommand) {
    if (!command) {
      return;
    }
    if (!command.action || !command.sound) {
      return;
    }
    let audioElm!: ElementRef;
    switch (command.sound) {
      case 'intro':
        audioElm = this.introSound;
        break;
      case 'click':
        audioElm = this.clickSound;
        break;
      case 'type':
        audioElm = this.typeSound;
        break;
      default:
        console.warn('Invalid sound');
    }
    switch (command.action) {
      case 'play':
        this.play(audioElm, command.miliseconds);
        break;
      case 'stop':
        this.stop(audioElm);
        break;
      default:
        console.warn('Invalid sound command');
    }
  }

  play(audioElm: ElementRef, time?: number) {
    audioElm.nativeElement.play().then( () => {
      if (time) {
        setTimeout(() => {
          this.stop(audioElm);
        }, time);
      }
    })
    .catch(() => {
      console.warn('Couldn\'t play the audio');
    });
  }

  stop(audioElm: ElementRef) {
    audioElm.nativeElement.pause();
  }

}
