import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SoundPlayerComponent } from './sound-player/sound-player.component';
import { ConsoleComponent } from './console/console.component';

@NgModule({
  declarations: [
    SoundPlayerComponent,
    ConsoleComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SoundPlayerComponent,
    ConsoleComponent
  ]
})
export class ComponentsModule { }
