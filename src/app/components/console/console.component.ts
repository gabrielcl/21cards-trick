import {
  Component,
  OnInit,
  SimpleChanges,
  Input,
  OnChanges
} from '@angular/core';
import { Timer } from 'src/app/util/timer';

@Component({
  selector: 'app-console',
  templateUrl: './console.component.html',
  styleUrls: ['./console.component.scss']
})
export class ConsoleComponent implements OnInit, OnChanges {

  @Input() msgToType!: string;

  message: string[][] = [];
  messageStorage: string[] = [];
  timer: any;

  currentParagraph = 0;
  lastParagraph = 1;

  ngOnChanges(changes: SimpleChanges) {
    if (changes.msgToType) {
      this.hurryLastParagraph();
      this.messageStorage.unshift(changes.msgToType.currentValue);
      const msg = changes.msgToType.currentValue.split('');
      this.typeMessage(msg);
    }
  }

  ngOnInit() {}

  typeMessage(message: string) {
    const constructingMsg: string[] = [];
    this.message.unshift(constructingMsg);
    let next = 0;
    this.timer = Timer(() => {
      constructingMsg.push(message[next]);
      next++;
      if (next === message.length) {
        this.timer.stop();
      }
      return Promise.resolve(true);
    }, 70);
    this.timer.start();
  }

  clearConsole() {
    if (this.timer) {
      this.timer.stop();
    }
    this.message = [];
    this.messageStorage = [];
  }

  hurryLastParagraph() {
    if (this.timer) {
      this.timer.stop();
    }
    if (this.message[this.lastParagraph]) {
      this.message[this.lastParagraph] = this.messageStorage[this.lastParagraph].split('');
    }
  }

}
